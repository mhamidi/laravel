<footer id="footer" class="navbar-default navbar-fixed-bottom">
    <div class="footer-top">
        <div class="container">
            <div class="row sidebar">
                <aside class="col-xs-12 col-sm-6 col-md-3 widget social">
                    <div class="title-block">
                        <h3 class="title">Follow Us</h3>
                    </div>
                    <p>Follow us in social media</p>
                    <div class="social-list">
                        <a class="icon rounded icon-facebook" href="#"><i class="fa fa-facebook"></i></a>
                        <a class="icon rounded icon-twitter" href="#"><i class="fa fa-twitter"></i></a>
                        <a class="icon rounded icon-google" href="#"><i class="fa fa-google"></i></a>
                        <a class="icon rounded icon-linkedin" href="#"><i class="fa fa-linkedin"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </aside>

                <aside class="col-xs-12 col-sm-6 col-md-3 widget newsletter">
                    <div class="title-block">
                        <h3 class="title">Newsletter Signup</h3>
                    </div>
                    <div>
                        <p>Sign up for newsletter</p>
                        <div class="clearfix"></div>
                        <form class="subscribe-form" method="post" action="">
                            <!--input class="form-control email" type="email" name="subscribe">
                            <button class="submit">
                              <span class="glyphicon glyphicon-arrow-right"></span>
                              </button-->
                            <div class="input-group margin-bottom-sm">
                                <span class="input-group-addon"><i class="fa fa-envelope-o fa-fw"></i></span>
                                <input class="form-control" type="text" placeholder="Email address">
                            </div>
                            <span class="form-message" style="display: none;"></span>
                        </form>
                    </div>
                </aside><!-- .newsletter -->

                <aside class="col-xs-12 col-sm-6 col-md-3 widget links">
                    <div class="title-block">
                        <h3 class="title">Information</h3>
                    </div>
                    <nav>
                        <ul>
                            <li><a href="#">About us</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Terms &amp; Condotions</a></li>
                            <li><a href="#">Secure payment</a></li>
                        </ul>
                    </nav>
                </aside>

                <aside class="col-xs-12 col-sm-6 col-md-3 widget links">
                    <div class="title-block">
                        <h3 class="title">My account</h3>
                    </div>
                    <nav>
                        <ul>
                            <li><a href="#">My account</a></li>
                            <li><a href="#">Order History</a></li>
                            <li><a href="#">Wish List</a></li>
                            <li><a href="#">Newsletter</a></li>
                        </ul>
                    </nav>
                </aside>
            </div>
        </div>
    </div><!-- .footer-top -->

    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="copyright col-xs-12 col-sm-3 col-md-3">
                    Copyright © AIAEXPERT, 2017
                </div>

                <div class="phone col-xs-6 col-sm-3 col-md-3">
                    <div class="footer-icon">
                        <i class="fa fa-mobile" aria-hidden="true"></i>
                        <strong>Call Us:</strong> +212 6 69 18 45 99<br>
                        <strong>or</strong> +212 6 52 74 59 22
                    </div>
                </div>

                <div class="address col-xs-6 col-sm-3 col-md-3">
                    <div class="footer-icon">
                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                        847E Lot. Adrar, Tikiouine, BP 80000 <br> AGADIR
                    </div>
                </div>

                <div class="col-xs-12 col-sm-3 col-md-3">
                    <a href="#" class="up">
                        <span class="fa fa-arrow-up"></span>
                    </a>
                </div>
            </div>
        </div>
    </div><!-- .footer-bottom -->
</footer>