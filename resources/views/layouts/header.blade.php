<a class="navbar-logo" href="#"></a>

<nav class="navbar-inverse nav-upper">
    <div class="call">
        <p><span>Need help?</span> call us <span class="number">1-22-3456789</span></span></p>
    </div>
    <div class="container-fluid">
        <ul class="nav navbar-upper">
            <li><a href="#">Register</a></li>
            <li><a href="#">Login</a></li>
            <li><a href="#">Delivery</a></li>
            <li><a href="#">Checkout</a></li>
            <li><a href="#">My Account</a></li>
        </ul>
    </div>
</nav>

<nav id="header" class="navbar navbar-inverse navbar-static-top navbar-lower">
    <div class="container-fluid">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false"> <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <div class="collapse navbar-collapse" id="navbar">
            <ul class="nav navbar-nav">
                <li><a href="#">NAV-B-1</a>
                </li>
                <li><a href="#">NAV-B-2</a>
                </li>
                <li><a href="#">NAV-B-3</a>
                </li>
                <li><a href="#">NAV-B-4</a>
                </li>
            </ul>
            <form class="navbar-form navbar-right" role="search">
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search">
                        <span class="input-group-btn">
                          <button type="submit" class="btn btn-default">Submit</button>
                        </span>
                    </div>
                </div>
            </form>
        </div>
    </div>
</nav>